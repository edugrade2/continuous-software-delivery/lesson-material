#!/usr/bin/bash
# A simple exporter utility called either manually or in a CI/CD pipeline.

# All top-level directories in this repo that starts with "chapter-" are
# considered to contain lesson material, and only those are considered for
# exporting.
for D in $(find . -maxdepth 1 -type d -wholename "./chapter-*");
do
    cd "$D";
    # Each lesson directory should contain a "dist" directory where the exports
    # are saved. If not, create it.
    if [[ ! -d "dist" ]]; then
          mkdir "dist";
    fi
    # Each .ipynb file in the lesson directory is exported in the following
    # formats:
    #   - slides (revealjs)
    #   - html
    #   - markdown
    for FILE in $(find . -maxdepth 1 -type f -name "*.ipynb"); do
        for format in $(echo -e "html slides markdown"); do
            jupyter nbconvert --to "$format" --output-dir "dist" "$FILE";
        done
    done
    # Copy over all images to the dist directory
    for FILE in $(find . -maxdepth 1 -type f -name "*.png"); do
        cp "$FILE" dist/;
    done
    # Get back to parent directory (since we use relative paths on the top loop)
    cd ../;
done
