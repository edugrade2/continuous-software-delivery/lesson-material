# Used to check and correct PEP8 compliancy
autopep8
# Linter
pylint
# Unit test framework
pytest
